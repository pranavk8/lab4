/****************************************************************************

  Header file for DecodeMorse service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServDecodeMorse_H
#define ServDecodeMorse_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitDecodeMorseService(uint8_t Priority);
bool PostDecodeMorseService(ES_Event_t ThisEvent);
ES_Event_t RunDecodeMorseService(ES_Event_t ThisEvent);

#endif /* ServDecodeMorse_H */

