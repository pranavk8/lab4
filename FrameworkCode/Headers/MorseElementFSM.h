/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef MorseElement_H
#define MorseElement_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitMorseElements, CalWaitForRise, CalWaitForFall,
  EOC_WaitRise, EOC_WaitFall, DecodeWaitRise, DecodeWaitFall
}MorseElementState_t;

// Public Function Prototypes

bool InitMorseElementFSM(uint8_t Priority);
bool PostMorseElementFSM(ES_Event_t ThisEvent);
ES_Event_t RunMorseElementFSM(ES_Event_t ThisEvent);
bool CheckMorseEvents(void);
void CharacterizeSpace(void);
MorseElementState_t QueryMorseElementFSM(void);

#endif /* FSMTemplate_H */

