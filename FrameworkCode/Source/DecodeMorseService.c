/****************************************************************************
 Module
   DecodeMorseService.c

 Revision
   1.0.1

 Description
   This is a DecodeMorse file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from DecodeMorseFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include <string.h>
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Events.h"
#include "DecodeMorseService.h"
#include "LCDService.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
char DecodeMorseString(void);
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static char MorseString[8];
static char LegalChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?.,:'-/()\"= ";
static char MorseCode[][8] ={ ".-","-...","-.-.","-..",".","..-.","--.",
                      "....","..",".---","-.-",".-..","--","-.","---",
                      ".--.","--.-",".-.","...","-","..-","...-",
                      ".--","-..-","-.--","--..",".----","..---",
                      "...--","....-",".....","-....","--...","---..",
                      "----.","-----","..--..",".-.-.-","--..--",
                      "---...",".----.","-....-","-..-.","-.--.-",
                      "-.--.-",".-..-.","-...-"
                     };

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDecodeMorseService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitDecodeMorseService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  MorseString[0] = '\0';
  /********************************************
   in here you write your initialization code
   *******************************************/
  return true;
}

/****************************************************************************
 Function
     PostDecodeMorseService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostDecodeMorseService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunDecodeMorseService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunDecodeMorseService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  //char space_ascii = ' '
  char DecodedChar;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   *******************************************/
  switch (ThisEvent.EventType) 
  {
    case DotDetected:
    {
      if(strlen(MorseString) < 7){
        strcat(MorseString, ".");
      }
      else{
        ReturnEvent.EventType = ES_ERROR;
        ReturnEvent.EventParam = 1;
      }
    }
    break;
    
    case DashDetected:
    {
      if(strlen(MorseString) < 7){
        strcat(MorseString, "-");
      }
      else{
        ReturnEvent.EventType = ES_ERROR;
        ReturnEvent.EventParam = 2;
      }
    }
    break;
    
    case EOCDetected:
    {
      printf("Detected EOC");
      DecodedChar = DecodeMorseString();
      ThisEvent.EventType = ES_LCD_PUTCHAR;
      ThisEvent.EventParam = DecodedChar;
      PostLCDService(ThisEvent);
      printf("Posting char to LCD");
      MorseString[0] = '\0';
    }
    break;
    
    case EOWDetected:
    {
      printf("Detected EOW");
      DecodedChar = DecodeMorseString();
      //strcat(&DecodedChar, " ");
      ThisEvent.EventType = ES_LCD_PUTCHAR;
      ThisEvent.EventParam = DecodedChar;
      PostLCDService(ThisEvent);
      
      ThisEvent.EventType = ES_LCD_PUTCHAR;
      ThisEvent.EventParam = ' ';
      PostLCDService(ThisEvent);
      printf("Posting char to LCD");
      MorseString[0] = '\0';
    }
    break;
    
    case BadSpace:
    {
      MorseString[0] = '\0';
    }
    break;
    
    case BadPulse:
    {
      MorseString[0] = '\0';
    }
    break;
    
    case DBButtonDown:
    {
      MorseString[0] = '\0';
    }
    break;
  }
  return ReturnEvent;
}

/************************************************************************
************************************************************************/

char DecodeMorseString(void){
  int i = 0;
  for(i = 0;i<=sizeof(MorseCode);i++){
    if(!strcmp(MorseString,MorseCode[i])){
      return LegalChars[i];
    }
  }
  return '~';
}
/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

