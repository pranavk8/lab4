/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"
#include "ES_Events.h"
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MorseElementFSM.h"
#include "DecodeMorseService.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static MorseElementState_t CurrentState ;
// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority, LastInputState;
static uint16_t TimeOfLastRise, TimeOfLastFall, LengthOfDot, FirstDelta;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitMorseElementFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  HWREG(SYSCTL_RCGCGPIO) |= BIT1HI;
  while((HWREG(SYSCTL_PRGPIO) & BIT1HI) != BIT1HI)
  {
  }
  //Enable and set port B bit 3 to input
  HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= BIT3HI;
  HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) &= BIT3LO;
  //Reading present value of PB4
  LastInputState = HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) & BIT3HI;
  //Initialize FirstDelta
  FirstDelta = 0;
  // put us into the Initial PseudoState
  CurrentState = InitMorseElements;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMorseElementFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/**********************************************************
Morse event checking function
*********************************************************/
bool CheckMorseEvents(void){
  bool ReturnVal = false;
  uint8_t CurrentInputState;
  ES_Event_t ThisEvent;
  CurrentInputState = HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) & BIT3HI;
  if(CurrentInputState != LastInputState){
    if(CurrentInputState != 0){
      ThisEvent.EventType = RisingEdge;
      ThisEvent.EventParam = ES_Timer_GetTime();
      PostMorseElementFSM(ThisEvent);
    }
    else{
      ThisEvent.EventType = FallingEdge;
      ThisEvent.EventParam = ES_Timer_GetTime();
      PostMorseElementFSM(ThisEvent);
    }
    ReturnVal = true;
  }
  LastInputState = CurrentInputState;
  return ReturnVal;
}

/*****************************************************************
Test Calibration function 
*/

void TestCalibration(void){
  uint16_t SecondDelta, PulseWidth;
  ES_Event_t ThisEvent;
  SecondDelta = 0;
  PulseWidth = TimeOfLastFall - TimeOfLastRise;
  if (FirstDelta == 0){
    FirstDelta = PulseWidth;
  }
  else{
    SecondDelta = PulseWidth;
    if((100*FirstDelta/SecondDelta) <= 33.33){
      LengthOfDot = FirstDelta;
      ThisEvent.EventType = CalibrationCompleted;
      PostMorseElementFSM(ThisEvent);
    }
    else if((100*FirstDelta/SecondDelta) >= 300){
      LengthOfDot = FirstDelta;
      ThisEvent.EventType = CalibrationCompleted;
      PostMorseElementFSM(ThisEvent);
    }
    else{
      FirstDelta = SecondDelta;
    }
  }
}
/***********************************************************************
Characterize Space function
***********************************************************************/
void CharacterizeSpace(void){
  uint16_t LastInterval;
  ES_Event_t Event2Post;
  LastInterval = TimeOfLastRise - TimeOfLastFall;
  
  if(LastInterval<(LengthOfDot-1) || LastInterval>(LengthOfDot+1)){
    if((LastInterval>=(3*LengthOfDot-1)) && (LastInterval<=(3*LengthOfDot+1))){
      Event2Post.EventType = EOCDetected;
      printf(" ");
      PostMorseElementFSM(Event2Post);
      if(CurrentState == DecodeWaitRise){
        PostDecodeMorseService(Event2Post);
      }
    }
    else {
      if(LastInterval>=(7*LengthOfDot-1) && LastInterval<=(7*LengthOfDot+1)){
      Event2Post.EventType = EOWDetected;
      printf("     ");
      if(CurrentState == DecodeWaitRise){
        PostDecodeMorseService(Event2Post);
       }
      }
      else{
      Event2Post.EventType = BadSpace;
      if(CurrentState == DecodeWaitRise){
        PostDecodeMorseService(Event2Post);
      }
      }
    }
  }
}
/***********************************************************************
Characterize Pulse function
***********************************************************************/
void CharacterizePulse(void){
	uint16_t LastPulseWidth;
  ES_Event_t Event2Post;
  
  LastPulseWidth = TimeOfLastFall - TimeOfLastRise;
  if((LastPulseWidth>=(LengthOfDot-1)) && (LastPulseWidth<=(LengthOfDot+1))){
      Event2Post.EventType = DotDetected;
      PostDecodeMorseService(Event2Post);
      printf(".");
  }
  else
  {
    if((LastPulseWidth>=(3*LengthOfDot-1)) && (LastPulseWidth<=(3*LengthOfDot+1))){
      Event2Post.EventType = DashDetected;
      PostDecodeMorseService(Event2Post);
      printf("-");
    }
    else{
      Event2Post.EventType = BadPulse;
      PostDecodeMorseService(Event2Post);
    }
  }
      
}

/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunMorseElementFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  MorseElementState_t NextState;
  NextState = CurrentState;
  switch (CurrentState)
  {
    case InitMorseElements:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        NextState = CalWaitForRise;
      }
    }
    break;

    case CalWaitForRise:        // If current state is state one
    {
      printf("In CalWaitForRise \r\n");
      if(ThisEvent.EventType == RisingEdge){
        TimeOfLastRise = ThisEvent.EventParam;
        printf("R\r\n");
        NextState = CalWaitForFall;
      }
      if(ThisEvent.EventType == CalibrationCompleted){
				//printf(FirstDelta);
        NextState = EOC_WaitRise;
      }
    }
    break;
    
    case CalWaitForFall:
    {
      printf("In CalWaitForFall \r\n");
      if(ThisEvent.EventType == FallingEdge){
        printf("F\r\n");
        TimeOfLastFall = ThisEvent.EventParam;
        NextState = CalWaitForRise;
        TestCalibration();
				printf("%u\r\n" , LengthOfDot);
      }
    }
    break;
    
    case EOC_WaitRise:
    {
      printf("In EOC_WaitRise \r\n");
      if(ThisEvent.EventType == RisingEdge){
        TimeOfLastRise = ThisEvent.EventParam;
        NextState = EOC_WaitFall;
        printf("Before CharacterizeSpace\r\n");
        CharacterizeSpace();
      }
      if(ThisEvent.EventType == DBButtonDown){
        //printf("ButtonDown detected \r\n");
        NextState = CalWaitForRise;
        FirstDelta = 0;
      }
    }
    break;
    
    case EOC_WaitFall:
    {
      //printf("In EOC_WaitFall \r\n");
      if(ThisEvent.EventType == FallingEdge){
        TimeOfLastFall = ThisEvent.EventParam;
        NextState = EOC_WaitRise;
      }
      if(ThisEvent.EventType == DBButtonDown){
        NextState = CalWaitForRise;
        FirstDelta = 0;
      }
      if(ThisEvent.EventType == EOCDetected){
        //printf("EOC detected in EOC_WaitFall \r\n");
        NextState = DecodeWaitFall;
      }
    }
    break;
    
    case DecodeWaitRise:
    {
      //printf("In DecodeWaitForRise \r\n");
      if(ThisEvent.EventType == RisingEdge){
        TimeOfLastRise = ThisEvent.EventParam;
        NextState = DecodeWaitFall;
        CharacterizeSpace();
      }
      if(ThisEvent.EventType == DBButtonDown){
        NextState = CalWaitForRise;
        FirstDelta = 0;
      }
    }
    break;
    
    case DecodeWaitFall:
    {
      //printf("In DecodeWaitForFall \r\n");
      if(ThisEvent.EventType == FallingEdge){
        TimeOfLastFall = ThisEvent.EventParam;
        NextState = DecodeWaitRise;
        CharacterizePulse();
      }
      if(ThisEvent.EventType == DBButtonDown){
        NextState = CalWaitForRise;
        FirstDelta = 0;
      }
    }
    break;
    // repeat state pattern as required for other states
    
  }                                   // end switch on Current State
  CurrentState = NextState;
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
MorseElementState_t QueryMorseElementFSM(void)
{
  return CurrentState;
}


/**************************************************************************
 private functions
***************************************************************************/

